import unittest
from py2neo import Graph, Node, Relationship
from mergeNodes import mergeNodes

class MergeNodeTestCase(unittest.TestCase):
    def setUp(self):
        print("Populating graph")
        g = Graph(password='scisynergy')
        if len(g):
            raise Exception("Graph database not empty, aborting ...")

        i1=Node("Institution", name="UnB")
        a1=Node("Author", name="Alba Cristina de Melo", authorid=1)
        a2=Node("Author", name="Alba C. M. de Melo", authorid=2)
        a3=Node("Author", name="Celia Ghedini Ralha", authorid=3)
        a4=Node("Author", name="Genaina Nunes Rodrigues", authorid=4)
        a5=Node("Author", name="Genaína Nunes Rodrigues", authorid=5)
        p1=Node("Publication", title="p1", year="2019", type="article")
        p2=Node("Publication", title="p2", year="2018", type="article")
        p3=Node("Publication", title="p3", year="2017", type="article")
        p4=Node("Publication", title="p4", year="2016", type="article")
        p5=Node("Publication", title="p5", year="2015", type="article")

        r1=Relationship(a1, "ASSOCIATED_TO", i1)
        r2=Relationship(a3, "ASSOCIATED_TO", i1)
        r3=Relationship(a4, "ASSOCIATED_TO", i1)
        r4=Relationship(a1, "AUTHORING", p1)
        r5=Relationship(a1, "AUTHORING", p2)
        r6=Relationship(a1, "AUTHORING", p3)
        r7=Relationship(a1, "AUTHORING", p4)
        r8=Relationship(a1, "AUTHORING", p5)
        r9=Relationship(a2, "AUTHORING", p5)
        r10=Relationship(a3, "AUTHORING", p5)
        r10=Relationship(a5, "AUTHORING", p4)

        for r in [r1,r2,r3,r4,r5,r6,r7,r8,r9,r10]:
            g.create(r)

        print("Done")

    def tearDown(self):
        g = Graph(password='scisynergy')
        g.delete_all()
        print("Graph cleared")
        
    def testMerge(self):
        print('Running testes')
        
        g = Graph(password="scisynergy")

        print("Authors",[x['name'] for x in g.nodes.match("Author")])
        print("Number of AUTHORING relations",
              len([x for x in g.relationships.match([], r_type="AUTHORING")]))

        g.run('CALL apoc.export.graphml.all("graph_before.graphml", {})')
        a1 = g.nodes.match("Author", authorid=1).first()
        
        a2 = g.nodes.match("Author", authorid=2).first()

        if a1 is None or a2 is None:
            raise Exception("Unable to merge with None")
        
        mergeNodes(g, a1, a2)

        print("Authors",[x['name'] for x in g.nodes.match("Author")])
        print("Number of AUTHORING relations",
              len([x for x in g.relationships.match([], r_type="AUTHORING")]))
        g.run('CALL apoc.export.graphml.all("graph_after.graphml", {})')
        

if __name__ == "__main__":
     unittest.main()
     
