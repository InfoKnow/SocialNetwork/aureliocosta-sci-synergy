from bs4 import BeautifulSoup as bs
import requests

print("Starting scraping of USP_dcc")

class ResearcherUSP(object):
    def __init__(self, name='', email=''):
        self.name = name
        self.email = email
    def toCypher(self):
        return "CREATE(a:Author {name: '" + self.name + "', email: '" + self.email + "'});"

url = 'http://www.ime.usp.br/dcc/pos/orientadores'
uspResearchers = list()

page = requests.get(url)
page.encoding="ISO-8859-1"

soup = bs(page.text, 'html.parser')
maincolumn = soup.find('div', attrs={'id':'maincolumn'})

for info_orientador in maincolumn.find_all('div', class_="info_orientador"):
    name = info_orientador.find('span', class_='destaque_nome').text
    raw_email = info_orientador.find_all('a')[-1]['href']
    email = raw_email.split(':')[1]
    uspResearchers.append(ResearcherUSP(name, email))

with open('docentes-usp.cypher','w', encoding='ISO-8859-1') as f:
    f.write("CREATE(n:Institution {name: 'USP', color: '#800000'});\n")
    for r in uspResearchers:
        f.write(r.toCypher()+ "\n")
        f.write("MATCH(i:Institution {name: 'USP'}),(a:Author {name: '"+r.name+"'}) MERGE (a)-[r:ASSOCIATED_TO]->(i);\n")    
print("Finished !!")