from bs4 import BeautifulSoup as bs
import requests

tiposProf = ['CO', 'PE', 'PA']
docentes=list()

class ResearcherUFMG(object):
    def __init__(self, name=''):
        self.name = name
    def toCypher(self):
        return "CREATE(a:Author {name: '" + self.name + "'});"


for tpProf in tiposProf:
    page = requests.get('https://www.dcc.ufmg.br/pos/pessoas/professores2.php?tipo=%s' % tpProf)
    page.encoding = 'ISO-8859-1'
    soup = bs(page.text, 'html.parser')
    profTbl=soup.find_all(attrs={'valign':'top','class':'td_sub_conteudo'})
    
    indice=1
    for profCell in profTbl:
        try:
            if tpProf == 'PE':
                docentes.append(ResearcherUFMG(profCell.strong.text))
                indice +=1
        except:
            pass


with open('docentes-ufmg.cypher','w', encoding='ISO-8859-1') as f:
    f.write("CREATE(n:Institution {name: 'UFMG', color: '#FFFF00'});\n")
    for r in docentes:
        f.write(r.toCypher()+ "\n")
        f.write("MATCH(i:Institution {name: 'UFMG'}),(a:Author {name: '"+r.name+"'}) MERGE (a)-[r:ASSOCIATED_TO]->(i);\n")    
print("Finished !!")