﻿#!/usr/bin/python
from bs4 import BeautifulSoup
import requests
import sys, re

print("Iniciando parser da pagina de docentes da UnB")

class Researcher(object):
    def __init__(self, nome='', email='', titulo='', areas='', titulo_ano_local='', lattes=''):
        self.name = nome
        self.email = email
        self.lattesurl = lattes
        self.titulo = titulo
        self.titulo_ano_local = titulo_ano_local
        self.areas = areas        
                
    #def __repr__(self):
    #    return '{"name": "' + self.name + '", "email": "' + self.email + '", "lattesurl": "' + self.lattesurl + '"}\n'
    def toCypher(self):
        return "CREATE(a:Author {name: '" + self.name + "', lattesurl: '" + self.lattesurl + "', email: '" + self.email + "', title: '" + self.titulo+"', title_when_where: '" + self.titulo_ano_local+"', areas: '" + self.areas + "'});"

try:
    pagina = requests.get("http://ppgi.unb.br/index.php?option=com_content&view=article&id=78&Itemid=471&lang=pt")
except Exception as e:
    print("Erro ao ler a pagina %s", str(e))
    sys.exit(1)

pagina.encoding="ISO-8859-1"
soup = BeautifulSoup(pagina.text, 'html.parser')
docentes = list()
tabela_docentes = soup.find('table', attrs={"class":"docentes"})

contador = 0
nome = ''
email = ''
areas = ''
titulo = ''
titulo_ano_local = ''
lattes = ''
for campo in tabela_docentes.find_all('td'):
    desloc = contador % 8
    if desloc == 1:
        nome = campo.text.split('(')[0].strip()
        lattes = campo.find_all('a')[-1]['href']
    if desloc == 2:
        titulo = campo.text.split(':')[1].strip()
    if desloc == 3:
        titulo_ano_local = campo.text
    if desloc == 4:
        email = campo.text.split(':')[1].strip().replace(" [at] ", "@")
    if desloc == 5:
        areas = campo.text.split(':')[1].strip()
        docentes.append(Researcher(nome, email, titulo, areas, titulo_ano_local, lattes))        
    contador += 1    

with open("docentes-unb.cypher",'w', encoding='ISO-8859-1') as f:
    f.write("CREATE(n:Institution {name: 'UnB', color:'#E466CB'});\n")
    for d in docentes:
        f.write(d.toCypher()+'\n')
        f.write("MATCH(i:Institution {name: 'UnB'}),(a:Author {name: '"+d.name+"'}) MERGE (a)-[r:ASSOCIATED_TO]->(i);\n")

print("Finished !!")