from py2neo import Graph, Node, Relationship


def mergeNodes(graph, n1, n2):
    if n1.labels != n2.labels:
        raise Exception("Node labels mismatch")
    
    alias = Node("Alias")

    # Create properties
    #for key in n1.keys():
    #    temp[key] = n1[key]
    alias['name'] = n2['name']
    # Create relations
    for r_type in graph.schema.relationship_types:
        for relation in graph.relationships.match([n1], r_type=r_type):
            print(n1, r_type, relation.end_node, relation.items())
            relationship_typed = Relationship.type(r_type)
            r = relationship_typed(n1, relation.end_node, dict(relation.items()))
            graph.create(r)    

    r = Relationship(alias, "ALIAS", n1)
    graph.create(r)    

    graph.delete(n2)


def mainmethod():
    g = Graph(password='scisynergy')

    print("Entre com o nome do pesquisador")
    name=input(">")

    query = """CALL db.index.fulltext.queryNodes('authorName', '%s')
                YIELD node
                RETURN node""" % name.replace(' ', ' AND ')

    authors = [x['node'] for x in g.run(query).data()]

    for i, record in zip(range(len(authors)), authors):
        print(i, record['name'])

    print("Escolha o indice do no primario")
    n1 = input(">")
    if n1 == "exit":
        print("Saindo ...")
        return
    print("Escolha o indice do no secundario")
    n2 = input(">")
    if n2 == "exit":
        print("Saindo ...")
        return
    mergeNodes(g, authors[int(n1)], authors[int(n2)])

if __name__ == "__main__":
    mainmethod()
