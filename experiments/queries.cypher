match (i:Institution {name: 'UnB'})-[]-(a:Author)-[]-(p:Publication) return p.year as year,count(p) order by year asc

------------------
MATCH 
    (i:Institution {name: 'UnB'})-[r1]-(a:Author)-[r2]-(p:Publication)
RETURN
    p.year as year, max(length(p.author)) order by year asc

-------------------------------
-- Union find or 
CALL algo.unionFind('Author', 'COAUTHOR', {write:true, partitionProperty:"partition"})
YIELD nodes, setCount, loadMillis, computeMillis, writeMillis;

-- Updata for GDS plugin
-- First we create a graph projection
call gds.graph.create('coauthoring', 'Author','COAUTHOR') YIELD graphName, nodeCount,relationshipCount, createMillis;

-- Call for wcc (weak connected component)
CALL gds.wcc.write('coauthoring', {nodeLabels: ['Author'], writeProperty:"partition"})
YIELD componentCount, createMillis, computeMillis, writeMillis, componentDistribution;

----------------------    
-- Compute betweenness centrality
CALL algo.betweenness('Author','COAUTHOR', {direction:'both',write:true, writeProperty:'centrality_b'})
YIELD nodes, minCentrality, maxCentrality, sumCentrality, loadMillis, computeMillis, writeMillis;

CALL gds.betweenness.write('coauthoring', {nodeLabels: ['Author'], writeProperty:'centrality_b'})
YIELD
  createMillis,
  computeMillis,
  writeMillis;


-----------------
--- Compute closeness centrality
--CALL algo.closeness('Author', 'COAUTHOR', {write:true, writeProperty:'centrality_c'})
--YIELD nodes,loadMillis, computeMillis, writeMillis;
-- With filter to the biggest partition
CALL algo.closeness('MATCH (n:Author) WHERE n.partition is not null RETURN id(n) AS id',
 'MATCH (n:Author) WHERE n.partition is not null WITH n.partition AS partid, count(n) AS count ORDER BY count DESC LIMIT 1 
 MATCH (a:Author)-[r:COAUTHOR]-(b:Author) WHERE id(a) < id(b) and a.partition = partid or b.partition = partid RETURN id(a) AS source, id(b) AS target',
 {write:true, writeProperty:'centrality_c', graph:'cypher'})
YIELD nodes,loadMillis, computeMillis, writeMillis;


CALL gds.alpha.closeness.write({
    nodeQuery: 'MATCH (p:Author) WHERE p.partition=0 RETURN id(p) AS id',
    relationshipQuery: 'MATCH (p1:Author)-[:COAUTHOR]->(p2:Author) WHERE p1.partition=0 AND p2.partition=0 RETURN id(p1) AS source, id(p2) AS target',
    writeProperty: 'centrality_c'})
    YIELD nodes, createMillis, computeMillis, writeMillis;

CALL gds.alpha.closeness.stream({
    nodeProjection: 'Author',
    relationshipProjection: 'COAUTHOR'
})
YIELD nodeId, centrality
RETURN gds.util.asNode(nodeId).name AS user, centrality
ORDER BY centrality DESC;

-----------------
MATCH (i:Institution {name: 'ufmg'})
set i.color = '#FFFF00'
return i    

--------------------
--- Não testado
MATCH (x:Institution)-[r1]-(a),
(y:Institution)-[r2]-(b)
WHERE x.name = y.name
AND id(x) < id(y)
CREATE y-[r1]-(a)
detach delete x


-- Cria a aresta de coautoria entre os autores
MATCH (a:Author)-[]-(p:Publication)-[]-(b:Author)
WHERE id(a) < id(b)
MERGE (a)-[r:COAUTHOR {year: p.year}]->(b)
RETURN r


------------------
-- Merge usando apoc.refacor.mergeNodes
MATCH (i:Publication)
WITH i.name AS name, COLLECT(i) AS nodeList, COUNT(*) as count
WHERE count > 1
CALL apoc.refactor.mergeNodes(nodeList) YIELD node
RETURN node

-----------------
MATCH (i1:Institution)-[]-(a1:Author)



MATCH path=(i1:Institution {name: 'UFAM'})-[]-(a1:Author)-[]-(p:Publication)-[]-(a2:Author)-[]-(i2:Institution)
                WHERE id(i1) = id(i2)
                RETURN p.title
                
MATCH p=(a1:Author)-[r:COAUTHOR]-(a2:Author) where a1.name =~ 'Genaina.*' return p

MATCH p=(i:Institution {name: 'UnB'})-[]-(a:Author) return p


----------------------------------------
-- from publication title to university
match path=(p:Publication {title: 'Multibody Interactions in Coarse-Graining Schemes for Extended Systems'})-[]-(a:Author)-[*]-(i:Institution) return path

-------------------------------
-- Constraints
CREATE CONSTRAINT ON (n:Author) ASSERT  n.name IS UNIQUE;
CREATE CONSTRAINT ON (n:Alias) ASSERT  n.name IS UNIQUE;
CREATE CONSTRAINT ON (n:Institution) ASSERT  n.name IS UNIQUE;
CREATE CONSTRAINT ON (n:Publication) ASSERT  n.title IS UNIQUE;

----------------------------------
match path=(a:Auhor)-[]-(p:Publication) where a.name =~ 'Genaina.*' return path


---------------------------------

MATCH (a:Author) where a.aliases is not null
FOREACH (alias in a.aliases | 
MERGE p=(a)<-[r:ALIAS]-(b:Alias {name: alias})
)
--------------------------------------------------------
-- Using fulltext search
CALL db.index.fulltext.createNodeIndex("authorName", ["Author"],["name"], {analyzer:"brazilian"});
CALL db.index.fulltext.createNodeIndex("authorAlias", ["Alias"],["name"], {analyzer:"brazilian"});

CALL db.index.fulltext.queryNodes("authorName", "firstname AND lastname") 
YIELD node, score 
RETURN node.name, score

CALL db.index.fulltext.drop("authorName");

#neo4j-backup.bat -to D:\devel\datagraph\backups\20191227\ -host localhost


------------------------------------------------------
-- Configura a propriedade authorid
MATCH (a:Author)
SET
    a.authorid = id(a)
return 
    a


---------------------------------------------
CALL algo.betweenness.stream('Author', 'COAUTHOR',
{direction:'out',concurrency:1})
YIELD nodeId, centrality 
with nodeId, centrality 
match (a:Author) 
where id(a) = nodeId 
return a.name, centrality 
 order by centrality desc
 limit 50

CALL algo.closeness.stream(label:String, relationship:String, {concurrency:4})
YIELD nodeId, centrality

--------------------------------------------------
-- Create user_id attribute
MATCH (n:Author) SET n.userid = id(n);


-- Show the 4 bigger partitions
match (a:Author) return a.partition, count(*) as componente order by componente desc limit 4;

-- Show how many author from eatch program are in biggest partition
match (i:Institution)--(a:Author) where a.partition = 8 return i.name, count(*);

-- Show coauthors from same institution (Institution is hard coded)
match 
    path=(a:Author {name: "Antonio Alfredo Ferreira Loureiro"})-[:COAUTHOR]-(b:Author) 
where 
    (:Institution {name: "UFMG"})--(b) 
return 
    distinct b.name