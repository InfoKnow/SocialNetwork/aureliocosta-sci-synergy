from bs4 import BeautifulSoup as bs
import requests
import sys

class ResearcherUFAM(object):
    def __init__(self, name=''):
        self.name = name
    def toCypher(self):
        return "CREATE(a:Author {name: '" + self.name + "'});"


class PPGIUFAMScrapper(object):
    def __init__(self):
        self.baseurl = "http://icomp.ufam.edu.br/site/index.php/institucional/sobre-icomp/corpo-docente"
        
    def scrap(self):
        page = requests.get(self.baseurl)
        #page.encoding = 'ISO-8859-1'
        soup = bs(page.text, "html5lib")

        div = soup.find('div', id="ja-content-main")
        if div is None:
            print("Main div not found")
            return
        professors = list()
        
        for inner in div.find_all('div', class_="contentpaneopen clearfix"):
            h2 = inner.find("h2", class_="contentheading")
            professors.append(ResearcherUFAM(h2.text.strip()))
        

        with open('docentes-ufam.cypher','w', encoding='ISO-8859-1') as f:
            f.write("CREATE(n:Institution {name: 'UFAM', color: '#00FFFF'});\n")
            for r in professors:
                f.write(r.toCypher()+ "\n")
                f.write("MATCH(i:Institution {name: 'UFAM'}),(a:Author {name: '"+r.name+"'}) MERGE (a)-[r:ASSOCIATED_TO]->(i);\n")

if __name__ == "__main__":
    print("Starting scrap for UFAM")
    scrapper = PPGIUFAMScrapper()
    scrapper.scrap()
    print("Finished !!")
