#-*- coding: utf-8 -*-
from bs4 import BeautifulSoup as bs
import requests
import sys

url = "https://sigaa.ufrn.br/sigaa/public/programa/equipe.jsf?lc=pt_BR&id=73"
professors=list()

class ResearcherUFRN(object):
    def __init__(self, name='', email='', lattes=''):
        self.name = " ".join([s.capitalize() for s in name.split()])
        self.email = email
        self.lattes = lattes
    def toCypher(self):
        if self.lattes != '':
            return "CREATE(a:Author {name: '" + self.name + "', email: '" + self.email + "', lattesurl: '" + self.lattes + "'});"
        else:
            return "CREATE(a:Author {name: '" + self.name + "', email: '" + self.email + "'});"

page = requests.get(url)
page.encoding = 'ISO-8859-1'
soup = bs(page.text, 'html.parser')

table = soup.find(id="table_lt")
if not table:
    print("No table found")
    sys.exit(-1)
for tr in table.find_all('tr'):
    if tr['class'] == 'campos':
        continue
    row = tr.find_all('td')
    name = row[0].text.strip()
    if name == "Nome":
        continue
    try:
        lattes_url = row[4].find('a')['href']
    except TypeError as e:
        print(str(e))
        lattes_url = None    
    email = row[5].find('a')['href'].split(':')[1]
    
    if lattes_url is not None:
        professors.append(ResearcherUFRN(name=name,email=email, lattes=lattes_url))
    else:
        professors.append(ResearcherUFRN(name=name, email=email))
        
with open("docentes-ufrn.cypher",'w', encoding='ISO-8859-1') as f:
    f.write("CREATE(n:Institution {name: 'UFRN', color: '#89F6C7'});\n")
    for p in professors:
        f.write(p.toCypher()+ "\n")
        f.write("MATCH(i:Institution {name: 'UFRN'}),(a:Author {name: '"+p.name+"'}) MERGE (a)-[r:ASSOCIATED_TO]->(i);\n")    

print("Finished !!")
        
