#!/bin/bash
function check_prereq
{
    nc -z datagraph 7687
}

while ! check_prereq;
do
    echo "Waiting required services become up"
    sleep 1
done

uwsgi --ini wsgi.ini