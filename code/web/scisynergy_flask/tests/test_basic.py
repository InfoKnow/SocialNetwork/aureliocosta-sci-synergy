import os
import unittest
 
from scisynergy_flask import app
from scisynergy_flask import models
 
class BasicTests(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()
        self.app.testing = True

    def test_main_page(self):
        response = self.app.get('/', follow_redirects=False)
        self.assertEqual(response.status_code, 200)
    
    def test_institutions_sort(self):
        institutions = [models.Institution().find_by_name(name) for name in models.Institution.getInstitutionsName()]
        institutions.sort(key=lambda i: len(i.get_authors()), reverse=True)
        for i in institutions:
            print(i.name)

    def test_maintenance(self):
        response = self.app.get('/maintenance')

        self.assertEqual(response.status_code, 200)


if __name__ == "__main__":
    unittest.main()