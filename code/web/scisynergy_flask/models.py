"""
Created on 20/04/2016

@author: aurelio
"""

from py2neo import Graph
from py2neo.data import Relationship
from py2neo.matching import RelationshipMatch
import os
import sys
from functools import lru_cache

graph = None

if os.environ.get('GAE_DEPLOYMENT_ID', 'bar') == 'bar':
    print('Using docker stack')
    try:
        graph = Graph(os.getenv('NEO4J_URI'),
                      user='neo4j', password='scisynergy')
    except IOError as e:
        print("!! No database available !!")
else:
    print('Using foreign database')
    try:
        graph = Graph(host='10.158.0.2', user='neo4j',
                      password='scisynergy', bolt=True)
    except Exception as err:
        print("Graph connection error: ", err)
        graph = ''


def verify_database():
    if len(graph) == 0:
        raise Exception("Empty Graph")


class Researcher(object):
    def __init__(self):
        self.publications = list()
        self.name = ''
        self.bagofareas = ''
        self.lattesurl = ''
        self.userid = ''
        self.recomendation = list()
        self.node = None
        self.institution = None

    def get_publications(self):
        pass

    @staticmethod
    def tokenSanitize(token):
        translationTable = str.maketrans(
            "çàáâãéèêíóòöõüñ", "caaaaeeeiooooun", ":'`{}[])(@?!_-/")
        return token.lower().translate(translationTable)

    def updateInfos(self, remote):
        """
        @desc Update user informations fetched from database
        """
        self.name = remote['name']
        self.bagofareas = remote['bagofareas']
        self.lattesurl = remote['lattesurl']
        self.userid = remote['userid']
        self.recomendation = list()
        self.node = remote

        for rec in RelationshipMatch(graph, nodes=[remote], r_type="RECOMMENDATION"):
            rec_area = rec.get('area')

            rec_str = rec.end_node['name']
            institution = ''
            if [x for x in self.recomendation if x.find(rec_str) >= 0]:
                continue
            for assoc in rec.end_node.match_outgoing("ASSOCIATED_TO"):
                institution = assoc.end_node['name']
            if rec.end_node['PQ'] is not None:
                rec_str += " (" + rec.end_node['PQ'] + ")"
            if institution != '':
                rec_str += ' - <b>' + institution.upper() + '</b>'

            if rec_area is not None:
                rec_str += " - Area: " + rec_area

            self.recomendation.append(rec_str)
        return self

    def find(self, idx=0):
        if idx is None or idx == 0:
            return None
        if graph == '':
            return None
        retval = None
        for author in graph.nodes.match("Author"):
            if author['userid'] == int(idx):
                retval = author

        if retval is not None:
            self.updateInfos(retval)
        return self

    @staticmethod
    def findByName(name):
        if not name or name == '':
            return None
        if graph == '':
            return None
        name = name.replace(' ', ' AND ').replace(';', '')

        query = 'call db.index.fulltext.queryNodes("authorName", "{}") yield node, score return node'.format(
            name)

        researchers = list()
        for r in graph.run(query).data():
            temp = Researcher()
            temp.updateInfos(r['node'])
            researchers.append(temp)
        return researchers

    @staticmethod
    def findByPartition(part_id):
        if not part_id or part_id == '':
            return None
        if graph == '':
            return None

        return [Researcher().updateInfos(author) for author in graph.nodes.match("Author", partition=int(part_id))]

    def findByInstitution(self, inst):
        authors = list()
        if inst is not None:
            authors = [rel for rel in graph.relationships.match(r_type="ASSOCIATED_TO") if
                       rel.end_node['name'] == inst]
        return [r.start_node['name'] for r in authors if r.start_node['name'] is not None]

    def findByFilter(self, filter):
        authors = list()
        if filter is None:
            raise Exception("Filter cant be empty")
        print("Buscando autores com o seguinte filtro: " + str(filter))
        query = "MATCH (r:Author)"
        if filter.get('institution') is not None:
            query += "-[:ASSOCIATED_TO]-(i:Institution{name: '" + \
                filter.get('institution')+"'})"
        query += " WHERE 1=1"
        if filter.get('researcher') is not None and filter.get('researcher') != '':
            query += " AND r.name = '" + filter.get('researcher') + "'"
        if filter.get("yearfrom") is not None or filter.get("yearfrom") != 0:
            pass
            #query += " AND toInteger(p.year) > toInteger(" + filter.get("yearfrom") + ")"
        if filter.get("yearto") is not None or filter.get("yearto") != 0:
            pass
            #query += " AND toInteger(p.year) < toInteger(" + filter.get("yearto") + ")"
        if filter.get("venuefilter_procee") is not None:
            pass
        if filter.get("venuefilter_jourar") is not None:
            pass
        if filter.get("venuefilter_books") is not None:
            pass

        query += " RETURN r"
        for researcher in graph.run(query).data():
            authors.append(researcher['r'].get('name'))
        return authors

    def get_institution_name(self):
        inst_name = ''
        for rel in RelationshipMatch(graph, nodes=[self.node], r_type="ASSOCIATED_TO"):
            inst_name = rel.end_node['name']
        return inst_name

    def get_coauthors_count(self):
        coauthors = set()
        for rel in RelationshipMatch(graph, nodes=[self.node], r_type="COAUTHOR"):
            coauthors.add(rel.end_node['name'])
        return len(coauthors)

    def get_aliases(self):
        ret_list = list()
        print("Procurando alias ...")

        for alias in RelationshipMatch(graph, r_type="ALIAS").where("a.name = '%s' OR b.name = '%s'" % (self.name, self.name)):
            print(alias)
            if alias.end_node == self.node:
                ret_list.append(alias.start_node['name'])
            else:
                ret_list.append(alias.end_node['name'])
        return ', '.join(ret_list)


class Publication(object):
    def __init__(self):
        super()

    def updateInfos(self, remote):
        self.title = remote['title']
        self.year = remote['year']
        self.author = remote['author']

    def find_by_author(self, author):
        publications = list()
        for rel in graph.relationships.match(nodes=[author], r_type="AUTHORING"):
            p = rel.end_node
            publications.append({
                'title': p['title'],
                'year': p['year'],
                'author': p['author']
            })
        return publications

    @staticmethod
    def find_by_type():
        types = dict()
        for publication in graph.nodes.match("Publication"):
            if types.get(publication['type'], 0) == 0:
                types[publication['type']] = 1
            else:
                types[publication['type']] = types[publication['type']] + 1

        types['Books'] = types.get('book', 0)
        types['Journal Articles'] = types.get('article', 0)
        types['In proceedings'] = types.get('inproceedings', 0)
        types.pop('phdthesis', None)
        types.pop('incollection', None)
        types.pop('book', None)
        types.pop('article', None)
        types.pop('inproceedings', None)

        return types

    def relationCoauthoring(self, filters=None):
        institution = filters['institution']
        researcher = filters['researcher']
        yearfrom = filters['timefrom']
        yearto = filters['timeto']
        proceedings = filters['venuefilter_procee']
        journals = filters['venuefilter_jourar']
        books = filters['venuefilter_books']

        query_filter = "a <> b"

        if researcher is not None and researcher != 'None':
            query_filter += " AND a.name = '" + researcher + "'"
        if int(yearfrom) != 0:
            query_filter += " AND toInteger(p.year) > " + yearfrom
        if int(yearto) != 0:
            query_filter += " AND toInteger(p.year) < " + yearto

        if proceedings != '0' or journals != '0' or books != '0':
            query_filter += " AND (1=1 AND "
            if proceedings != '0':
                query_filter += "p.type = 'inproceedings' OR "
            if journals != '0':
                query_filter += "p.type = 'article' OR "
            if books != '0':
                query_filter += "p.type = 'book' OR "
            query_filter += "1=1) "

        query_relations = f'''MATCH 
    (a:Author)-[r1:AUTHORING]-(p:Publication)-[r2:AUTHORING]-(b:Author) 
WHERE 
    {query_filter} 
WITH
    p,a,b
OPTIONAL MATCH 
    (a)-[rv:ASSOCIATED_TO]-(i:Institution)
RETURN a, b, count(p) as c, i'''

        if institution is not None and institution != 'all':
            query_relations = f'''MATCH 
    (i:Institution {{name: "{institution}"}})-[r1:ASSOCIATED_TO]-(a:Author)-[r2:AUTHORING]-(p:Publication)-[r3:AUTHORING]-(b:Author) 
WHERE 
    {query_filter} 
RETURN a, b, count(p) as c, i'''

        print("!!!Query relations", query_relations)
        relations = []
        for record in graph.run(query_relations):
            relations.append(record)
        
        links = list()
        if len(relations) < 1:
            raise Exception("Few nodes")
        for r in relations:
            links.append({"source": r['a']["name"],
                          "target": r['b']["name"],
                          "size": 6,
                          "rel_count": r['c'],
                          "color": r['i']['color'] if r['i'] is not None else "yellow",
                          "st": (r['a']["name"] + r['b']["name"]).replace(' ', '')
                          })
        return dict(children=links)

    @staticmethod
    def relationAuthorPublication():
        query_relations = "MATCH (a:Author)<-[r:AUTHORING]->(p:Publication) WHERE p.type='article' RETURN a,p"
        relations = graph.run(query_relations)

        links = list()
        if len(relations) < 1:
            raise Exception("Few nodes")
        for r in relations:
            links.append({"source": r[0]["name"],
                          "target": r[1]["key"],
                          "size": 6
                          })
        return dict(children=links)


class Institution(object):
    def __init__(self):
        self.name = ''
        self.node = ''

    def get_authors(self):
        authors = [rel for rel in graph.relationships.match(
            r_type="ASSOCIATED_TO") if rel.end_node['name'] == self.name]
        return authors

    def get_author_count(self):
        return len(self.get_authors)

    def find_by_name(self, name):
        if not name or name == '':
            return None
        if graph == '':
            return None
        retval = graph.nodes.match('Institution', name=name).first()
        if retval is not None:
            self.updateInfos(retval)

        return self

    def find_by_id(self, idx):
        if not idx or idx < 0:
            return None
        if graph == '':
            return None
        retval = graph.nodes.match('Institution', _id=idx).first()
        if retval is not None:
            self.updateInfos(retval)

        return self

    def updateInfos(self, remote):
        """
        @desc Update user informations fetched from database
        """
        self.name = remote['name']
        self.color = remote['color']
        self.node = remote
        return self

    def get_coauthorship_count(self, since=1700):
        query = '''
        MATCH 
    (i:Institution {name: '%s'})-[r1]-(a:Author)-[r2]-(p:Publication)-[r3]-(a2:Author)
    WHERE toInteger(p.year) >= %i
    AND r2 <> r3
WITH
    p.year as year, 
    a,
    count(distinct a2) as counter 
RETURN
    year,
    max(counter) as max,
    min(counter) as min,
    toInteger(avg(counter)) as media
    order by year asc
        ''' % (self.name, int(since))
        coauthorship_count = graph.run(query)

        return coauthorship_count.data()

    @staticmethod
    def getInstitutionsName():
        return [inst['name'] for inst in graph.nodes.match("Institution")]

    @staticmethod
    def institutionInteraction():
        query = '''MATCH
                    path=(i1:Institution)-[]-(a1:Author)-[]-(p:Publication)-[]-(a2:Author)-[]-(i2:Institution)
                WHERE 
                    id(i1) <= id(i2)
                RETURN 
                    i1.name,
                    i2.name,
                    i1.color,
                    i2.color,
                    count(p)
                '''
        interaction = graph.run(query)
        interlist = list()
        for i in interaction:
            source = i[0]
            source_label = source
            source_color = i[2]
            target = i[1]
            target_label = target
            target_color = i[3]
            rel_count = i[4]

            if source == target:
                source = source+'1'

            st = source+target
            interlist.append({"source": source,
                              "source_label": source_label,
                              "target": target,
                              "target_label": target_label,
                              "rel_count": rel_count,
                              "source_color": source_color,
                              "target_color": target_color,
                              "st": st}
                             )
        return dict(children=interlist)


class GraphInfo(object):
    cache = dict()

    def __init__(self):
        if type(graph) is str:
            raise RuntimeError("Graph not connected")

    @staticmethod
    def node_count(node_label=''):
        return graph.run('''
            MATCH 
                (n{}) 
            RETURN 
                count(n) 
            AS 
                nodes'''.format(':'+node_label if node_label != '' else '')
            ).data()[0]['nodes']

    @staticmethod
    def rel_count(rel_type=''):
        return graph.run('''
            MATCH 
                (a)-[r{}]-(b) 
            WHERE 
                id(a) < id(b) 
            RETURN 
                count(r) AS rels'''.format(':'+rel_type if rel_type != '' else '')
            ).data()[0]['rels']

    @staticmethod
    def author_count():
        return len(graph.nodes.match("Author"))

    @staticmethod
    def seed_count():
        return graph.run('''
            MATCH 
                (i:Institution)-[r]-(a:Author) 
            RETURN 
                count(r) 
            AS 
                seeds'''
            ).data()[0]['seeds']

    @staticmethod
    def publication_count():
        return len(graph.nodes.match("Publication"))

    @staticmethod
    def institution_count():
        return len(graph.nodes.match("Institution"))

    @staticmethod
    def avg_degree():
        num_nodes = GraphInfo.node_count('Author')
        num_rels = GraphInfo.rel_count('COAUTHOR')
        return format(num_rels / num_nodes, '.2f')

    @staticmethod
    def max_degree():
        return graph.run('''MATCH 
        (a:Author)-[r:COAUTHOR]-(b:Author) 
        WHERE id(a) < id(b) 
        RETURN a.name, count(r) AS coauthoring 
        ORDER BY coauthoring DESC 
        LIMIT 1''').data()[0]['coauthoring']

    @staticmethod
    def min_degree():
        for researcher in graph.nodes.match("Author"):
            pass
    
    @staticmethod
    def show_degree_by_institution():
        query = '''
        MATCH 
	p=(i:Institution)-[]-(a:Author) 
WITH 
	i, 
	collect(a) as authors 
UNWIND 
	authors as author 
MATCH 
	p=(author)-[r:COAUTHOR]-(b:Author) 
WITH 
	i, 
	authors,
	count(distinct b) as coa,
	count(r) as rels 
RETURN 
	i.name as Institution, 
	rels as Relationships, 
	size(authors) as Authors, 
	coa as Coauthors, 
	avg(toFloat(rels)/(size(authors) + coa)) as avg_degree 
ORDER BY
	avg_degree 
DESC
'''
        return graph.run(query).to_table()._repr_html_()

    @staticmethod
    def betweenness(limit=10):
        cursor = graph.run(f"""
        MATCH 
            (a:Author) 
        WHERE 
            a.centrality_b is not null 
        RETURN
             a.name as name, 
             a.centrality_b as betweenness 
        ORDER BY betweenness DESC 
        limit {limit};"""
                           ).data()
        return cursor

    @staticmethod
    def closeness(limit=10):
        cursor = graph.run(f"""match (a:Author) 
        where a.centrality_c is not null 
        return a.name as name, a.centrality_c as closeness 
        order by closeness desc limit {limit};"""
                           ).data()
        return cursor

    @staticmethod
    def diameter(limit=20):
        if GraphInfo.cache.get('diameter', 0) != 0:
            return GraphInfo.cache.get('diameter')

        query = """
        {}
""".format(limit)

        query = None
        cursor = None
        return cursor

    @staticmethod
    def shortest_path(limit=20):
        if GraphInfo.cache.get('shortest_path', 0) != 0:
            return GraphInfo.cache.get('shortest_path')

        query = f"""
        CALL 
algo.allShortestPaths.stream('year',{{nodeQuery:'Author',relationshipQuery:'MATCH (n:Author)-[r:COAUTHOR]-(p:Author) 
RETURN id(n) as source, id(p) as target, r.year as weight',defaultValue:1.0}})
YIELD sourceNodeId, targetNodeId, distance
WITH sourceNodeId, targetNodeId, distance
WHERE algo.isFinite(distance) = true

MATCH (source:Author) WHERE id(source) = sourceNodeId
MATCH (target:Author) WHERE id(target) = targetNodeId
WITH source, target, distance WHERE source <> target

RETURN source.name AS source, target.name AS target, distance
ORDER BY distance DESC
LIMIT {limit};
        """
        query = None
        #cursor = graph.run(query).data()
        cursor = None
        return cursor

    @staticmethod
    def union_find(limit=10):
        query = f"""
        MATCH 
            (a:Author)
        WHERE 
            a.partition is not null
        RETURN
            a.partition as partition, 
            count(*) as component_size 
            ORDER BY component_size desc 
            limit {limit}
        """

        cursor = graph.run(query).data()
        return cursor

    @staticmethod
    def partition_members(partition_id):
        query = f"""
        MATCH 
            (a:Author)
        WHERE
            a.partition = {partition_id}
        RETURN
            a.name AS name
        """

        return graph.run(query).data()

    @lru_cache(maxsize=1024)
    def distance(self, x, y):
        pass
