"""
Created on 07/03/2016

@author: aurelio
"""

import json
from flask import render_template, request, session, redirect, url_for, send_from_directory
from flask_table import Table, Col, LinkCol, create_table

import os
from scisynergy_flask import app
from .db import findArea
from .models import Researcher, GraphInfo
from flask import jsonify
from scisynergy_flask.models import Publication, Institution


class RawCol(Col):
    def td_format(self, content):
        return content
class ItemTable(Table):
    classes = ["mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp"]
    name = Col('Name')
    count = Col('Score')

class ClickableItemTable(Table):
    classes = ["mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp"]
    name = LinkCol('Name', 'view_full_table', url_kwargs=dict(id='name'), )
    count = Col('Score')


class Item(object):
    def __init__(self, name, count):
        self.name = name
        self.count = format(float(count), '.4f')

class ItemInt(object):
    def __init__(self, name, count):
        self.name = name
        self.count = "{:,}".format(int(count)).replace(',','.')

def num_of_authors(i):
    return len(i.get_authors())

@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route('/')
@app.route('/index')
def index():
    try:
        gi = GraphInfo()
    except Exception as err:
        app.error_message = err
        return redirect(url_for('maintenance'))

    institutions = [Institution().find_by_name(name) for name in Institution.getInstitutionsName()]
    institutions.sort(key=num_of_authors, reverse=True)
    
    return render_template('home.html',
                           graph_info={'node_count': gi.node_count(),
                                       'rel_count': gi.rel_count(),
                                       'author_count': gi.author_count(),
                                       'seed_count': gi.seed_count(),
                                       'publication_count': gi.publication_count(),
                                       'institution_count': gi.institution_count()
                                       },
                           pub_info=Publication.find_by_type(),
                           author_info={inst.name:len(inst.get_authors()) for inst in institutions}
                           )


@app.route('/pubgraph', methods=['POST', 'GET'])
def showgraph():
    inst = {i: i for i in Institution().getInstitutionsName()}
    graph_visible = False

    filters = dict()
    inst_selected = 'UnB'
    filters['institution'] = inst_selected
    filters['researcher'] = None
    filters['timefrom'] = 0
    filters['timeto'] = 0
    filters['venuefilter_procee'] = 0
    filters['venuefilter_jourar'] = 0
    filters['venuefilter_books'] = 0
    if request.method == 'POST':
        graph_visible = True
        try:
            filters['institution'] = request.form.get('institution')
            filters['researcher'] = request.form.get('researcher')
            filters['timefrom'] = request.form.get('timefrom', 0)
            filters['timeto'] = request.form.get('timeto', 0)
            filters['venuefilter_procee'] = request.form.get('venuefilter_procee', 0)
            filters['venuefilter_jourar'] = request.form.get('venuefilter_jourar', 0)
            filters['venuefilter_books'] = request.form.get('venuefilter_books', 0)
        except KeyError as e:
            print("Filter missing: " + str(e))
    
    researchers = Researcher().findByInstitution(filters['institution'])
    researchers.sort()

    if filters['institution'] is not None:
        inst_selected = filters['institution']

    return render_template('graphview.html',
        institutions=inst, 
        selected=inst_selected, 
        researchers=researchers, 
        filters=filters,
        graph_visible=graph_visible, 
        title_page="Graph")


@app.route('/api.json')
def graphapi():
    inst_filter = dict()
    inst_filter['institution'] = request.args.get('institution')
    inst_filter['researcher'] = request.args.get('researcher')
    inst_filter['timefrom'] = request.args.get('timefrom')
    inst_filter['timeto'] = request.args.get('timeto')
    inst_filter['venuefilter_procee'] = request.args.get('venuefilter_procee')
    inst_filter['venuefilter_jourar'] = request.args.get('venuefilter_jourar')
    inst_filter['venuefilter_books'] = request.args.get('venuefilter_books')
    return jsonify(Publication().relationCoauthoring(inst_filter))


@app.route('/interaction.json')
def interaction():
    return jsonify(Institution().institutionInteraction())


@app.route('/pubhistogram.json/<user_id>')
def publication_histogram(user_id):
    pass


@app.route('/recommending', methods=['POST', 'GET'])
def recommending():
    if request.method == 'POST':
        name = request.form['name']
        r = Researcher().findByName(name)
        
        return render_template('recommending.html', recos=r)
    else:
        return render_template('recommending.html', recos=None)


@app.route('/acmtree')
def acmtree():
    return render_template('acmtree.html')

@app.route('/api/v1/researcher/<string:inst_name>/<int:rec_id>')
def researcher(inst_name, rec_id):
    institution = Institution().find_by_name(inst_name)
    names = [a.start_node['name'] for a in institution.get_authors()]
    return jsonify(names)

@app.route('/api/v1/institution/<inst_name>')
def institution(inst_name):
    #institution = Institution().find_by_id(inst_id)
    institution = Institution().find_by_name(inst_name)

    return jsonify({'name': institution.name, 'color': institution.color})


@app.route('/profile/<user_id>')
def show_profile(user_id):
    if not user_id or int(user_id) < 0:
        return 'Invalid user id ', 400
    researcher = Researcher().find(user_id)
    pubs = Publication().find_by_author(researcher.node)
    coauthors = [(author,pub.get('year')) for pub in pubs for author in pub['author'] if author != researcher.name]

    index_by_year = dict()
    for pair in coauthors:
        if pair[1] in index_by_year:
            index_by_year[pair[1]].append(pair[0])
        else:        
            index_by_year[pair[1]] = [pair[0]]

    if researcher is None:
        return "User not found", 404
    
    institution = Institution().find_by_name(researcher.get_institution_name())
    authorship_count = None
    if institution is not None:
        try:
            authorship_count = institution.get_coauthorship_count(min(index_by_year.keys()))
        except ValueError:
            authorship_count = 0
    
    return render_template('profile.html',
                           title_page="Profile",
                           user=researcher,
                           publication=sorted(pubs, reverse=True, key=lambda a: a['year']),
                           coauthors=index_by_year,
                           total_coauthors=researcher.get_coauthors_count(),                           
                           coauthorship = authorship_count
                           )


@app.route('/search', methods=['GET', 'POST'])
def search():
    if request.method == 'POST':
        queryterm = request.form['searchkey']
        hits = Researcher.findByName(queryterm)       
                 
        if len(hits) == 0:
            return render_template("search.html", action='result', result=None, queryTerm=queryterm)
        else:            
            return render_template("search.html", action='result', result=hits, title_page="Result")
    else:
        return render_template('search.html', action='query', title_page="Search")

def linkfy_resource(identificator, type):
    if type == "partition":
        return '<a href="/full_table?centrality=unionfind&partition={}">{}</a>'.format(identificator, identificator)
    else:
        return identificator

@app.route('/statistics', methods=['GET'])
def stats():
    closenness = list()
    betweenness = list()
    tableCls = create_table("unionFindDataTable"
                ).add_column('partition',
                    RawCol('Component Id')
                ).add_column('component_size',
                    Col('Members')
                )
    table_uf = tableCls(
        [{'partition': linkfy_resource(record['partition'], 'partition'), 'component_size': record['component_size']} for record in GraphInfo.union_find()],
             border=True, 
            classes=["mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp"])
    
    for record in GraphInfo.closeness():
        closenness.append(Item(record['name'], record['closeness']))
    table_cc = ItemTable(closenness, border=True)
    
    for record in GraphInfo.betweenness():
        betweenness.append(ItemInt(record['name'], record['betweenness']))
    table_bc = ItemTable(betweenness, border=True)

    table_degree = GraphInfo.show_degree_by_institution()

    return render_template("statistics.html",
                           title_page="Statistics",
                           table_uf=table_uf,
                           table_cc=table_cc,
                           table_bc=table_bc,
                           table_degree=table_degree,                           
                           avg_degree=GraphInfo.avg_degree(),
                           max_degree=GraphInfo.max_degree()
                           )

@app.route("/full_table")
def view_full_table():
    centrality = request.args.get('centrality')
    if centrality == 'betweenness':
        betweenness = list()
        for record in GraphInfo.betweenness(500):
            betweenness.append(Item(record['name'], record['betweenness']))
        table = ItemTable(betweenness, border=True)
        return table.__html__()
    elif centrality == 'closeness':
        closenness = list()
        for record in GraphInfo.closeness(500):
            closenness.append(Item(record['name'], record['closeness']))
        table = ItemTable(closenness, border=True)
        return table.__html__()
    elif centrality == 'unionfind':
        partition_id = request.args.get('partition')
        tableCls = create_table(
            "componentMembers").add_column('#', Col('#')).add_column('name', 
                                        Col('Researcher')).add_column('institution', Col('Institution'))            
        table = tableCls(
                    [{'#': i+1 ,
                            'name': r.name, 
                            'institution': r.get_institution_name()} for i, r in enumerate(Researcher.findByPartition(partition_id))], 
                    border=True, 
                    classes=["mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp"]
                )
        
        return table.__html__()
    else:
        return "404"

@app.route('/faq')
def faq():
    return render_template("faq.html")

@app.route('/license')
def license():
    return render_template("license.html")

@app.route('/chord', methods=["GET"])
def chord():
    return render_template("chord.html")


@app.route('/autocomplete', methods=["GET"])
def autocomplete():
        partial = request.args.get('partial')        
        area = findArea(partial)
        
        return json.dumps(area)


@app.route('/loaddata', methods=['GET', 'POST'])
def loaddata():
    return render_template("load.html")


@app.errorhandler(404)
def page_not_found(error):
    return "A pagina solicitada nao esta disponivel", 404


@app.route('/maintenance')
def maintenance():
    try:
        return render_template('maintenance.html', error_message=app.error_message)
    except AttributeError:
        return render_template('maintenance.html')
