'''
Created on 07/03/2016

@author: aurelio
'''

from flask import Flask, g
from flask_material import Material
from contextlib import closing
import json, re, os
from .config import Config

app = Flask(__name__)
Material(app)
app.config.from_object(Config)

app.secret_key = '#@$dqeqwe12e11w1cc'



#@app.before_first_request
def extrat_acm_area_names():    
    json_raw = open(os.path.join('scisynergy_flask',os.path.join('static', 'acmtree.json')), 'r').read()
    prog = re.compile(r'"name": "([^"]+)"')
    areasName = prog.findall(json_raw, re.IGNORECASE)
    setattr(app, 'acmareas', areasName)

#@app.after_request
#def add_header(r):
#    r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
#    r.headers["Pragma"] = "no-cache"
#    r.headers["Expires"] = "0"
#    return r

import scisynergy_flask.views

if __name__ == '__main__':
    pass
