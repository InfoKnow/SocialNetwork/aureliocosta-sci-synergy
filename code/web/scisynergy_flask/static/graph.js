var nodes = {};
//var jsonURL = "/api.json?inst=" + institution;
var jsonURL = apiURL;
var links, path;


d3.json(jsonURL, function(json) {
  links = json.children;

  //Toggle children on click.
  function click(d) {
      if (d3.event.defaultPrevented) return; // click suppressed
      centerNode(d);
  }
  
  var zoomable_layer;

   // Compute the distinct nodes from the links.
  links.forEach(function(link) {
    link.source = nodes[link.source] || (nodes[link.source] = {name: link.source, label: link.source_label, size: link.size, type: "source", color: link.source_color || link.color});
    link.target = nodes[link.target] || (nodes[link.target] = {name: link.target, label: link.target_label, size: link.size, type: "target", color: link.target_color || link.color});
  });


  //Function to center node when clicked/dropped so node doesn't get lost when collapsing/moving with large amount of children.
  function centerNode(source) {
    scale = zoomListener.scale();
          x = -source.y0;
          y = -source.x0;
          x = x * scale + width / 2;
          y = y * scale + height / 2;
          d3.select('g').transition()
              .duration(duration)
              .attr("transform", "translate(" + x + "," + y + ")scale(" + scale + ")");
          zoomListener.scale(scale);
          zoomListener.translate([x, y]);
      }

      //define the zoomListener which calls the zoom function on the "zoom" event constrained within the scaleExtents
      var zoomListener = d3.zoom().scaleExtent([0.2, 3]).on("zoom", function() {        
        return zoomable_layer.attr("transform", d3.event.transform.translate(width/2, height/2));
        });

      var fLinks = d3.forceLink(links);
      fLinks.distance(50);
      var force = d3.forceSimulation()
          .nodes(d3.values(nodes))
          .force("link", fLinks)
          .force("charge", d3.forceManyBody().strength(-200))
          .on("tick", tick);

      var svg = d3.select("#myGraph").append("svg")
          .attr("width", width)
          .attr("height", height)
          .attr("class", "bg")
          .call(zoomListener);
      zoomable_layer = svg.append('g').attr("transform", "translate("+width/2+", "+height/2+") scale("+initial_scale+","+initial_scale+")");

      var link = zoomable_layer
        .attr("class", "links")
        .selectAll("path")
        .data(links)
        .enter().append("svg:path")
        .attr("stroke-width", function(d) { return 1 })
        .attr("class", function(d) { return "link " + d.st; })
        .attr("id",function(d,i) { return "linkId_" + i; })
        .attr("marker-end", function(d) { return "url(#" + d.st + ")"; });

      link.style('fill', 'none')
        .style('stroke', 'light-gray')
        .style("stroke-width", function(d) {return Math.log(d.rel_count + 1) + 'px'});


      var linktext = zoomable_layer.selectAll("linklabel").data(links);
     linktext.enter() 
     .append("text")
     .attr("class", "linklabel")
     .attr("x", function(d) {return "2vw"})
     .attr("y", function(d) {return ".31em"})
     .attr("text-anchor", "middle")
     .append("textPath")
    .attr("href",function(d,i) { return "#linkId_" + i;})
     .text(function(d) {
         return d.rel_count;
     });

      var node = zoomable_layer //.append('g')
        .attr("class", "nodes")
        .attr("height", height)
        .attr("width", width)
        .selectAll("g")
        .data(d3.values(nodes))
        .enter().append("g")
        .style('transform-origin', '50% 50%')
        .call(d3.drag()
          .on("start", dragstarted)
          .on("drag", dragged)
          .on("end", dragended)
        );

      
/*
      node.append("polygon")
        .attr("class", function(d) { return d.type; })
        .attr("points", function(d) {
            return moveXY(d.x, d.y);
        })
        .style("fill", function(d) {return d.institution || randomColor(); })
        .style("stroke-width", "2px")
        .attr("id", function(d) { return d.id; });
*/
        node.append("circle")
        .attr("class", function(d) { return d.type; })
            .attr("r", radius)
            .style("fill", function(d) {return d.color || "#FFFFFF"; })
            .style("stroke-width", "1px")
            .style("stroke", "ghostwhite")
        .attr("id", function(d) { return d.id; });

      node.append("text")
        .attr("dy", ".35em")
        .attr("class", "nodelabel")
        .attr("text-anchor", "middle")
        //.style("font-size", "1.2em")
        .text(function(d) { return d.label || d.name; });

      // Use elliptical arc path segments to doubly-encode directionality.
      function tick() {
        node.attr("transform", transform);        
        link.attr("d", linkArc);
        /*
        link.attr("x1", function(d) { return d.source.x; })
        .attr("y1", function(d) { return d.source.y; })
        .attr("x2", function(d) { return d.target.x; })
        .attr("y2", function(d) { return d.target.y; });
        */
      }

      function transform(d) {
        var x = Math.max(radius, Math.min(width - radius, d.x));
        var y = Math.max(radius, Math.min(height - radius, d.y));
        return "translate(" + d.x + "," + d.y + ")";
      }

      function linkArc(d) {
        var dx = d.target.x - d.source.x,
            dy = d.target.y - d.source.y,
            dr = Math.sqrt(dx * dx + dy * dy);
        //return "M" + d.source.x + "," + d.source.y + "A" + dr + "," + dr + " 0 0,1 " + d.target.x + "," + d.target.y;
        return "M" + d.source.x + "," + d.source.y + "L" + d.target.x + "," + d.target.y;
      }

      function moveXY(x,y) {
        var p1,p2,p3,p4,p5;
        p1 = (x + radius) + "," + y;
        p2 = (x + 2 * radius) + "," + (y + 0.9 * radius);
        p3 = (x + 1.8 * radius) + "," + (y + 2 * radius);
        p4 = (x + 0.2 * radius) + "," + (y + 2 * radius);
        p5 = x + "," + (y + 0.9 * radius);

        return p1 + " " + p2 + " " + p3 + " " + p4 + " " + p5;
      }

      function dragstarted(d) {
        if (!d3.event.active) force.alphaTarget(0.3).restart();
        d.fx = d.x;
        d.fy = d.y;
      }

      function dragged(d) {
        d.fx = d3.event.x;
        d.fy = d3.event.y;
      }

      function dragended(d) {
        if (!d3.event.active) force.alphaTarget(0);
        d.fx = null;
        d.fy = null;
      }
});
