
// gridlines in x axis function
function make_x_gridlines(x) {		
    return d3.axisBottom(x)
        .ticks(5)
}

// gridlines in y axis function
function make_y_gridlines(y) {		
    return d3.axisLeft(y)
        .ticks(5)
}

function drawHistogram(datum, dst, dots=0){
    // set the dimensions and margins of the graph
    var margin = {top: 20, right: 20, bottom: 30, left: 40},
    width = 1000 - margin.left - margin.right,
    height = 400 - margin.top - margin.bottom;

    // set the ranges
    var x = d3.scaleBand()
        .range([0, width])
        .padding(0.1);
    var y = d3.scaleLinear()
        .range([height, 0]);

    var svg = d3.select(dst).append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform",
        "translate(" + margin.left + "," + margin.top + ")");

    var data = {};
    datum.forEach(function(d){
        if(data[d.year] === undefined){
            data[d.year] = 1;
        }else{
            data[d.year] = data[d.year] + 1;
        }
    });

    var dataList = Array();
    Object.entries(data).forEach(function(d){
        dataList.push({year: d[0], count: d[1]});
    });


    var y_max = 0;
    
    if(dots) {
        y_max = d3.max(dots, function(d) { return d.max; });
    } else {
        y_max = d3.max(dataList, function(d) { return d.count; });
    }
    // Scale the range of the data in the domains
    x.domain(dataList.map(function(d){return d.year}));
    y.domain([0, y_max]);

    // add the X gridlines
  svg.append("g")			
  .attr("class", "grid")
  .attr("transform", "translate(0," + height + ")")
  .call(make_x_gridlines(x)
      .tickSize(-height)
      .tickFormat("")
  )

// add the Y gridlines
svg.append("g")			
  .attr("class", "grid")
  .call(make_y_gridlines(y)
      .tickSize(-width)
      .tickFormat("")
  )


    // append the rectangles for the bar chart
    svg.selectAll(".bar")
    .data(dataList)
    .enter().append("rect")
    .attr("class", "bar")
    .attr("x", function(d) { return x(d.year); })
    .attr("width", x.bandwidth())
    .attr("y", function(d) { return y(d.count); })
    .attr("height", function(d) { return height - y(d.count); });

    if (dots) {
        svg.selectAll(".dot")
        .data(dots)
        .enter().append("circle") // Uses the enter().append() method
        .attr("class", "dotMin") // Assign a class for styling
        .attr("cx", function(d, i) { return x(d.year)+25 })
        .attr("cy", function(d) { return y(d.min) })
        .attr("r", 5)

        svg.selectAll(".dot")
        .data(dots)
        .enter().append("circle") // Uses the enter().append() method
        .attr("class", "dotAvg") // Assign a class for styling
        .attr("cx", function(d, i) { return x(d.year)+25 })
        .attr("cy", function(d) { return y(d.media) })
        .attr("r", 5)

        svg.selectAll(".dot")
        .data(dots)
        .enter().append("circle") // Uses the enter().append() method
        .attr("class", "dotMax") // Assign a class for styling
        .attr("cx", function(d, i) { return x(d.year)+25 })
        .attr("cy", function(d) { return y(d.max) })
        .attr("r", 5);
    }    

    // add the x Axis
    svg.append("g")
    .attr("transform", "translate(0," + height + ")")
    .call(d3.axisBottom(x));

    // add the y Axis
    svg.append("g")
    .call(d3.axisLeft(y).ticks(y_max).tickFormat(d3.format("d")));

}

function drawLineChart(datum, dst){
    /* base on: https://bl.ocks.org/pstuffa/26363646c478b2028d36e7274cedefa6 */
    var d3_selected = d3.select(dst);
    var margin = {top: 5, right: 5, bottom: 5, left: 5}
  , width = 400 - margin.left - margin.right // Use the window's width 
  , height = 200 - margin.top - margin.bottom; // Use the window's height

  var n = 21;

  var xScale = d3.scaleLinear()
    .domain([0, n-1]) // input
    .range([0, width]); // output

    var yScale = d3.scaleLinear()
    .domain([0, 1]) // input 
    .range([height, 0]); // output 

    var line = d3.line()
    .x(function(d, i) { return xScale(i); }) // set the x values for the line generator
    .y(function(d) { return yScale(d.y); }) // set the y values for the line generator 
    .curve(d3.curveMonotoneX) // apply smoothing to the line

    var dataset = d3.range(n).map(function(d) { return {"y": d3.randomUniform(1)() } })


var svg = d3_selected.append("svg")
.attr("width", width + margin.left + margin.right)
.attr("height", height + margin.top + margin.bottom)
.append("g")
.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

svg.append("g")
.attr("class", "x axis")
.attr("transform", "translate(0," + height + ")")
.call(d3.axisBottom(xScale)); // Create an axis component with d3.axisBottom

svg.append("g")
.attr("class", "y axis")
.call(d3.axisLeft(yScale)); // Create an axis component with d3.axisLeft

svg.append("path")
.datum(dataset) // 10. Binds data to the line 
.attr("class", "line") // Assign a class for styling 
.attr("d", line); // 11. Calls the line generator 


svg.selectAll(".dot")
.data(dataset)
.enter().append("circle") // Uses the enter().append() method
.attr("class", "dot") // Assign a class for styling
.attr("cx", function(d, i) { return xScale(i) })
.attr("cy", function(d) { return yScale(d.y) })
.attr("r", 5);

}