//source: http://www.bscalable.com/blog-galapagos/2015/6/27/web-charts-with-d3js

var width = 170;
var height = 170;
var radius = Math.min(width, height) / 2;
var color = d3.schemecategory20;
var pie = d3.pie().sort(null);
var arc = d3.arc()
    .innerRadius(radius - 50)
    .outerRadius(radius - 35);

function randomColor(){
    var letters = '0123456789ABCDEF';
    var color = '#';

    for(var i = 0; i < 6; i++){
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}


function drawDonut(d, i){
    var researchers = d3.select(this).attr("researchers");
    var total_res = d3.select(this).attr("authors");
    var inst_name = d3.select(this).attr("institution");
    var percent_res = (researchers * 100) / total_res;

    var colorUrl = "/api/v1/institution/" + inst_name;
    var color = '#000000';

    

    var svg = d3.select(this)
        .append("svg")
        .attr("width", width)
        .attr("height", height)
        .append("g")
        .attr("transform", "translate("
            + width / 2.090 + ","
            + height / 2  + ")");
    /*
    fetch(colorUrl,{method: 'GET'})
          .then(response => {
              return response.json();
          }).then(data => {
              color = data.color;
          });
    */
   d3.json(colorUrl, function(d){
    color = d.color;


    var path = svg.selectAll("path")
        .data(pie([100 - percent_res, percent_res ]))
        .enter().append("path")
        .attr("stroke", "black")
        .attr("stroke-width", ".8")
        .attr("fill", function(d, i) {
            if (i == 0)
                return "#FFFFFF";
            else                
                return color;
        })
        .attr("d", arc);

    svg.append("svg:text")
        .attr("dy", ".30em")
        .attr("text-anchor", "middle")
        .attr("style","font-family:Ubuntu")
        .attr("font-size","20")
        .attr("fill","#000000")
        .text(inst_name);

    svg.append("svg:text")
        .attr("dy", "15%")
        .attr("text-anchor", "middle")
        .attr("style","font-family:Ubuntu")
        .attr("font-size","15")
        .attr("fill","#000000")
        .text(researchers);
    })
}

d3.selectAll("#donut").each(drawDonut);