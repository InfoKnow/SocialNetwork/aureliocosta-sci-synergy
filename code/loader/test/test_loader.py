import unittest
import sys
import os
import subprocess
import signal
import time
from functools import wraps
from py2neo import Graph, Node

from multiprocessing import Process

sys.path.append(os.getcwd()+'\\code\\loader\\dblploader')

#print(sys.path)

from DBLPParseNeo25 import runAlgorithms, insertIntoGraph

fake_pubs = [{'title': "1", 
                'author':['x','t','s'], 
                'type': 'inproceedings', 
                'year':2016
            },
            {'title': "2", 
                'author':['s','d','w'], 
                'type': 'book', 
                'year':2016
            },
            {'title': "3", 
                'author':['y','g','c'], 
                'type': 
                'article', 
                'year':1986
            },
            {'title': "A semiotic engineering approach to user interface design.", 
                'author':['y','g','c'], 
                'type': 'inproceedings', 
                'year':1986
            }]

def start_neo4j():
    subprocess.Popen(["D:\\devel\\datagraph\\neo4j-community-3.3.4\\bin\\neo4j.bat", "console"], stdin=subprocess.PIPE)

def clear_neo4j():
    pass

def timeit(method):
    @wraps(method)
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        if 'log_time' in kw:
            name = kw.get('log_name', method.__name__.upper())
            kw['log_time'][name] = int((te - ts) * 1000)
        else:
            print('%r  %2.2f ms' % (method.__name__, (te - ts) * 1000))
        return result
    return timed

class TestLoader(unittest.TestCase):
    
    def setUp(self):
        #time.sleep(3)
        pass
    
    def tearDown(self):
        pass

    @timeit    
    def test_run_algorithms(self):
        graph = None
        self.assertIsNone(runAlgorithms())

    @timeit
    def test_simple_insert(self):
        graph = Graph(password='scisynergy')
        author = Node.cast(
            {'name': 'John Smith',
            'email':'john@smith.com'
            })
        author.add_label("Author")
        graph.create(author)

        
    @timeit
    def test_duplicatedInsert(self):
        # Insert publicatio p
        # Try to insert p again
        p1 = Node.cast({
            'title': 'Holy Bible',
            'author': ['Peter', 'Metheus', 'John'],
            'year': 0,
            'type': 'book'
        })
        p1.add_label("Publication")

        p2 = Node.cast({
            'title': 'Holy Bible',
            'author': ['Peter', 'Metheus', 'John'],
            'year': 0,
            'type': 'book'
        })
        p2.add_label("Publication")

        graph = Graph(password='scisynergy')
        
        graph.run("CREATE CONSTRAINT ON (pub:Publication) ASSERT pub.title IS UNIQUE")
        graph.create(p1)
        graph.create(p2)
        
    @timeit
    def test_insertIntoGraph(self):
        for publication in fake_pubs:            
            insertIntoGraph(publication)
            
if __name__ == "__main__":
    #p = Process(target=start_neo4j)
    #p.start()
    
    #unittest.main(exit=False)
    suite = unittest.TestLoader().loadTestsFromTestCase(TestLoader)
    unittest.TextTestRunner(verbosity=2).run(suite)

    #p.terminate()
    #p.join(5)
    #if p.is_alive():
    #    print("Killing ", str(p.pid))
    #    os.kill(p.pid, signal.SIGTERM)
    #    time.sleep(10)
    # To run
    # C:\Python36\python.exe -m cProfile .\code\loader\test\test_loader.py >out.profile