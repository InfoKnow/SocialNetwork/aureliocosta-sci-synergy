import gzip
from xml.sax import make_parser, handler
from xml.sax.handler import feature_external_ges
from dblploader.dblpcontent import DBLPContentHandler
import time
import datetime
from py2neo import Graph, Node, Relationship
from py2neo.database.work import ClientError
import os
import json
import requests

file_source = 'dblp.xml.gz'
graph = Graph(password='scisynergy')
cql_available = True

CONFIG = ''
with open(os.path.join("code", "loader","dblploader","configloader.json")) as configFile:
    CONFIG = json.load(configFile)

def downloadDBLPdump():
    # Skip if xml file already exists
    if os.path.exists(CONFIG['dblp']['gzfile']):
        print("Xml database already exists, skipping download")
        return
        
    for file in [CONFIG['dblp']['dtdfile'], CONFIG['dblp']['gzfile']]:
        print("Starting download of " + CONFIG['dblp']['url'] + file)
        
        response = requests.get(CONFIG['dblp']['url'] + file)
        
        if response.status_code != requests.codes.ok:
            raise IOError("Server response: " + str(response.status_code))
        with open(file, "wb", buffering=0) as data:
            for chunk in response.iter_content(chunk_size=128):
                data.write(chunk)
        
        print("Download of", file, "finished")

#================================================================
def loadAuthorFilter():
    """
    @description Load author seeds from json filter and insert them in graph 
    @return The list of author names loaded as seeds
    """
    if cql_available:
        for dir_entry in os.listdir():
            if dir_entry.find(".cypher") > 0:
                with open(dir_entry, 'r', encoding='latin-1') as seed:
                    while True:
                        line = seed.readline()
                        if line is None or len(line) == 0:
                            break
                        try:
                            graph.run(line)
                        except ClientError as err:
                            print("Erro loading author", err)
                        finally:
                            line = ''
    else:        
        profList = list()
        
        filterFiles = ['docentes-unb.json',
                       'docentes-ufmg.json',
                       'docentes-ufrn.json',
                       'docentes-usp.json',
                       'docentes-ufam.json']        
        
        for j in filterFiles:
            print("Loading filter %s" % j)
            instName = j.split('.')[0].split('-')[1]

            institution = graph.nodes.match("Institution", name=instName)
            if institution is None:
                institution = Node("Institution", name=instName)
                for p in json.load(open(j, 'r', encoding='latin-1')):
                    if p is not None:
                        author = graph.nodes.match("Author", name=p['name'])
                        if author.first() is not None:
                            continue
                        author = Node("Author", name=p['name'],
                                      lattesurl=p['lattesurl'])
                        
                        graph.create(author)
                        graph.create(Relationship(author, "ASSOCIATED_TO", institution))
                        # del author
                        profList.append(author)
                        
            else:
                print("\tFilter load SKIPPED")
                for rel in institution.match(rel_type='ASSOCIATED_TO'):
                    profList.append(rel.start_node())
            del institution
        
        return profList

def content_cb(attrs):        
    if attrs['key'].find("homepages") >= 0:
        name_set = set(attrs.get("author", list()))        
        for author in authors:
            if author['name'] in name_set:                
                author['aliases'] = list(name_set)
                for name in name_set:
                    alias = Node("Alias", name=name)
                    rel = Relationship(alias, "ALIAS", author)
                    graph.create(rel)


print("Start initial load", time.asctime())
start_time = time.time()

downloadDBLPdump()
print("Loading seeds ...")
loadAuthorFilter()
authors = [n for n in graph.nodes.match('Author')]

with gzip.open(file_source, mode='rt', encoding='latin-1') as source:
    parser = make_parser()     
    parser.setContentHandler(DBLPContentHandler(callback=content_cb))
    parser.setFeature(feature_external_ges, True)
    print("Parsing ...")
    parser.parse(source)

elapsed_time = time.time() - start_time
print("Execution time ", datetime.timedelta(seconds=elapsed_time))
print("Done")