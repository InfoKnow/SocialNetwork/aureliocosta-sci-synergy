import datetime
import gzip
import time
import os
from xml.sax import ContentHandler, make_parser
from xml.sax.handler import feature_validation, feature_external_ges
import pika
import json
import requests
import logging
from logging.handlers import RotatingFileHandler

logging.basicConfig(
    format='%(name)s - %(levelname)s - %(message)s',
    handlers=[RotatingFileHandler('event_producer.log', maxBytes=10000000, backupCount=5)],
    level=logging.INFO)
logger = logging.getLogger("Producer log")


CONFIG = ''
with open(os.path.join("code", "loader","dblploader","configloader.json")) as configFile:
    CONFIG = json.load(configFile)

class DBLPContentHandler(ContentHandler):
    inPublication = True
    currentPubName = ''
    attrs = {}
    value = ''

    def __init__(self):
        
        super()
        self.allowedtypes = ['www',
            'inproceedings',
            'proceedings',
            'book',
            'incollection',
            'phdthesis',
            'article',
            'mastersthesis']

    
    def startDocument(self):
        self.pubAttrs = ''
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(host='localhost')
            )
        channel = connection.channel()

        [channel.queue_declare(queue=q, durable=True) for q in self.allowedtypes]
        self.connection = connection
        self.channel = channel

    def endDocument(self):
        self.connection.close()

    def startElement(self, name, attrs):
        if name in self.allowedtypes:
                DBLPContentHandler.inPublication = True
                DBLPContentHandler.currentPubName = name
                DBLPContentHandler.attrs['key'] = attrs['key']
                DBLPContentHandler.attrs['furthertype'] = attrs.get('publtype', '')

    def endElement(self, name):
        if DBLPContentHandler.inPublication is True:
            if DBLPContentHandler.currentPubName == name:
                DBLPContentHandler.attrs["type"] = name
                if DBLPContentHandler.attrs['furthertype'] != 'informal' or name != 'www':
                    msg_body = bytes(str(DBLPContentHandler.attrs), encoding='utf-8') 
                    logger.info(f"Enqueuing {msg_body}")                   
                    self.channel.basic_publish(exchange='', 
                                routing_key=name, 
                                body=msg_body, 
                                properties=pika.BasicProperties(delivery_mode=2)
                            )
                    
                # Flush object                
                DBLPContentHandler.inPublication = False
                DBLPContentHandler.attrs = {}
            else:
                if name == "author":
                    if DBLPContentHandler.attrs.get(name) is not None:
                        DBLPContentHandler.attrs[name].append(DBLPContentHandler.value.strip())
                    else:
                        DBLPContentHandler.attrs[name] = [DBLPContentHandler.value]
                else:
                    DBLPContentHandler.attrs[name] = DBLPContentHandler.value
        DBLPContentHandler.value = ''
        
    def characters(self, content):
        if content != '':
            DBLPContentHandler.value += content.replace('\n', '')
        
    
with gzip.open("dblp.xml.gz", mode='rt', encoding='latin-1') as fp:
    parser = make_parser()
    
    start_time = time.time()
    parser.setContentHandler(DBLPContentHandler())
    parser.setFeature(feature_external_ges, True)
    parser.parse(fp)
    
    elapsed = time.time() - start_time
    print("File parser in ", datetime.timedelta(seconds=elapsed))
