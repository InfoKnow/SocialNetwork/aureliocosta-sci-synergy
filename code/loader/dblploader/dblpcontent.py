from xml.sax import ContentHandler
import logging

class DBLPContentHandler(ContentHandler):
    """Handle xml database content"""
    inPublication = True
    currentPubName = ''
    attrs = {}
    value = ''

    def __init__(self, queue=None, pool = None, callback=None, pubtype = None):
        super()
        
        self.queue = queue
        self.callback = callback
        self.pool = pool
        self.pubtype = pubtype
        self.allowedtypes = ['www',
            'inproceedings',
            'proceedings',
            'book',
            'incollection',
            'phdthesis',
            'article',
            'mastersthesis']
        
        
    def startElement(self, name, attrs):
        try:
            if name in self.allowedtypes:
                DBLPContentHandler.inPublication = True
                DBLPContentHandler.currentPubName = name
                DBLPContentHandler.attrs['key'] = attrs['key']
        except ValueError as error:
            logging.info(error)

    def endElement(self, name):
        if DBLPContentHandler.inPublication is True:            
            if DBLPContentHandler.currentPubName == name:
                DBLPContentHandler.attrs["type"] = name
                
                if name not in self.allowedtypes:
                    print("Type not allowed", name)
                    DBLPContentHandler.inPublication = False
                    DBLPContentHandler.attrs = {}
                    return
            
                if self.queue is not None:
                    self.queue.put(DBLPContentHandler.attrs)
                else:
                    if self.pool is not None:
                        self.pool.apply_async(self.callback, args=(DBLPContentHandler.attrs,)).get()
                    else:                        
                        self.callback(DBLPContentHandler.attrs)
                # Flush object                
                DBLPContentHandler.inPublication = False
                DBLPContentHandler.attrs = {}
            else:
                if name == "author":
                    if DBLPContentHandler.attrs.get(name) is not None:
                        DBLPContentHandler.attrs[name].append(DBLPContentHandler.value.strip())
                    else:
                        DBLPContentHandler.attrs[name] = [DBLPContentHandler.value]
                else:
                    DBLPContentHandler.attrs[name] = DBLPContentHandler.value
        DBLPContentHandler.value = ''

    def characters(self, content):
        if content != '':
            DBLPContentHandler.value += content.replace('\n', '')
    def endDocument(self):
        if self.queue is not None:
            self.queue.close()