import pika
import json
from cachetools import cached
from cachetools.keys import hashkey
from py2neo import Graph, NodeMatcher, Node, Relationship
import os
import re
import time
import argparse
import logging
from logging.handlers import RotatingFileHandler

logging.basicConfig(
    format='%(name)s - %(levelname)s - %(message)s',
    handlers=[RotatingFileHandler('event_consumer.log', maxBytes=10000000, backupCount=5)],
    level=logging.INFO)
logger = logging.getLogger("Consumer log")

def wait_neo4j_connection(max_retry:int = 50) -> Graph:
    local_graph = None
    for count in range(max_retry):
        try:
            local_graph = Graph(CONFIG['neo4j']['url'],
              user=CONFIG['neo4j']['username'],
              password=CONFIG['neo4j']['password']
             )
            local_graph.schema.node_labels
            break
        except Exception as err:
            print(f"Erro trying({count}) to connect to neo4j: {err}")
            local_graph=None
            time.sleep(1)
    if local_graph is None:
        raise Exception("Neo4J database unavailable")
    return local_graph


print("Sci-Synergy Publication Event Consumer")
CONFIG = ''
with open(os.path.join("code", "loader","dblploader","configloader.json")) as configFile:
    CONFIG = json.load(configFile)

allowedtypes = ['www',
            'inproceedings',
            'proceedings',
            'book',
            'incollection',
            'phdthesis',
            'article',
            'mastersthesis']

local_graph = wait_neo4j_connection()


@cached(cache={}, key=lambda graph, name: hashkey(name))
def search_author_by_alias(graph: Graph, name: str) -> Node:
    alias = graph.nodes.match("Alias", name = name).first()
    if alias is None:
        return None
    else:
        return graph.relationships.match(nodes={alias}, r_type="ALIAS").first().end_node
        

def callback(ch, method, properties, body):
    logger.debug(f"Received message {body}")

    local_graph = wait_neo4j_connection()    
    pubEvent = eval(body.decode('utf-8'))

    if pubEvent.get('type') == 'www':
        ch.basic_ack(delivery_tag=method.delivery_tag)
        return

    matcher = NodeMatcher(local_graph)

    tx = local_graph.begin()
    publication = matcher.match("Publication",
        title = pubEvent.get('title')
        ).first()
    if publication is None:
        publication = Node('Publication', **pubEvent)
        #publication.__primarylabel__ = 'Publication'
        publication.__primarykey__ = 'title'
        publication.add_label('Publication')
    else:
        ch.basic_ack(delivery_tag=method.delivery_tag)
        del tx
        return
    
    authors = publication.get('author')
    if not isinstance(authors, list):
        authors = list()
        authors.append(publication.get('author'))

    can_insert = False
    for name in authors:
        if search_author_by_alias(local_graph, name) is not None:
            can_insert = True

    if can_insert is True:
        for name in authors:
            name = re.sub(" \d{3,4}$", "", name)
            name = re.sub('"', "''", name)
            author = matcher.match("Author").where("toLower(_.name) = \"%s\"" % name.lower()).first()
            if author is None:
                alias = matcher.match("Alias").where("toLower(_.name) = \"%s\"" % name.lower()).first()
                if alias is None:
                    author = Node("Author", name=name)
                    author.__primarylabel__ = 'Author'
                    author.__primarykey__ = 'name'
                    authoring = Relationship(author, "AUTHORING", publication)
                    local_graph.create(authoring)
                else:
                    author = local_graph.relationships.match(nodes={alias}, r_type='ALIAS').first().end_node
                    authoring = Relationship(author, "AUTHORING", publication)
                    local_graph.create(authoring)
            else:
                authoring = Relationship(author, "AUTHORING", publication)
                local_graph.create(authoring)
    local_graph.commit(tx)
    ch.basic_ack(delivery_tag=method.delivery_tag)

opt_parser = argparse.ArgumentParser(description="Consume msgs from queues and insert them in neo4j")
opt_parser.add_argument("--queues", dest="queues")

args = opt_parser.parse_args()

queues = args.queues.split(',')

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost')
    )
channel = connection.channel()
channel.basic_qos(prefetch_count=1)
[channel.queue_declare(queue=q, durable=True) for q in allowedtypes]

for queue in queues:
    if queue not in allowedtypes:
        raise Exception(f"Queue {queue} not identified")
    else:
        print(f"Starting consumer for queue {queue}")
        channel.basic_consume(
            queue=queue, 
            on_message_callback=callback
        )

print("Hit Ctrl+C to quit")
while True:
    try:
        channel.start_consuming()
    except pika.exceptions.StreamLostError as err:
        print("Connection to rabbitmq lost, taking some rest before try again. Exception:",err)
    except pika.exceptions.ChannelWrongStateError as err:
        print("Inconsistent connection state of rabbitmq, taking some rest before try again. Exception:",err)
    finally:
        time.sleep(1)
        connection = pika.BlockingConnection(
                        pika.ConnectionParameters(host='localhost')
                    )
        channel = connection.channel()
