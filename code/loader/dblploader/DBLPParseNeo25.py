"""Script to import data from dblp xml to neo4j graph database"""
from xml.sax import make_parser, handler
import gzip
import time
import datetime
import json
import uuid
import multiprocessing as mp
import sys
import shutil
import os
import logging
from logging.handlers import RotatingFileHandler
from dblpcontent import DBLPContentHandler
from functools import lru_cache
# nltk está gerando um erro pois não está encontrando os drives D e E
#from nltk import distance
from py2neo import Graph, Node, Relationship, NodeMatcher
import requests

CONFIG = ''

with open("code\\loader\\dblploader\\configloader.json") as configFile:
    CONFIG = json.load(configFile)

FORMAT = '%(asctime)-15s %(processName)s:  %(message)s'
logger = logging.getLogger(__name__)
logging.basicConfig(format=FORMAT, level=logging.INFO, handlers=[logging.FileHandler("DBLPImport.log", 'w', 'cp1252')])
#logHandler = RotatingFileHandler("DBLPImport.log", 'a', maxBytes=5000000, backupCount=10)
#logger.addHandler(logHandler)
#logger.setLevel('INFO')

URL = CONFIG['dblp']['gzfile']

synonyms_list = list()

cql_available = True

rootRepo = CONFIG['repo']['root']
counter = 0

if not cql_available:
    # Create repository of cypher files
    if not os.path.exists(rootRepo):
        logger.info("Creating repo dir " + rootRepo)
        os.mkdir(rootRepo)
    else:
        if os.listdir(rootRepo) is not None:
            logger.info("Removing old files")
            shutil.rmtree(rootRepo)
            logger.info("Recreating repo dir: " + rootRepo)
            os.mkdir(rootRepo)

#================================================================
def downloadDBLPdump():
    # Skip if xml file already exists
    if os.path.exists(URL):
        logger.info("Xml database already exists, skipping download")
        return
        
    for file in [CONFIG['dblp']['dtdfile'], CONFIG['dblp']['gzfile']]:
        logger.info("Starting download of " + CONFIG['dblp']['url'] + file)
        
        response = requests.get(CONFIG['dblp']['url'] + file)
        
        if response.status_code != requests.codes.ok:
            raise IOError("Server response: " + str(response.status_code))
        with open(file, "wb", buffering=0) as data:
            for chunk in response.iter_content(chunk_size=128):
                data.write(chunk)
        
        logger.info("Download of " + file + " finished")

#================================================================
def loadSeeds():
    """
    @description Load author seeds from json filter and insert them in graph 
    @return The list of author names loaded as seeds
    """
    graph = Graph(CONFIG['neo4j']['url'],
              user=CONFIG['neo4j']['username'],
              password=CONFIG['neo4j']['password'],
              bolt=True
             )
    if cql_available:
        #TODO: Missing update aliases field
        for dir_entry in os.listdir():
            if dir_entry.find("cypher") > 0:
                with open(dir_entry, 'r', encoding='latin-1') as seed:
                    while True:
                        line = seed.readline()
                        if line is None or len(line) == 0:
                            break
                        graph.run(line)
                        line = ''
        for author in graph.nodes.match("Author"):
            for synonyms in synonyms_list:
                            if author['name'] in synonyms:
                                author['aliases'] = synonyms
                                graph.pull(author)
    else:        
        profList = list()
        
        filterFiles = ['docentes-unb.json',
                       'docentes-ufmg.json',
                       'docentes-ufrn.json',
                       'docentes-usp.json',
                       'docentes-ufam.json']        
        
        for j in filterFiles:
            print("Loading filter %s" % j)
            instName = j.split('.')[0].split('-')[1]

            institution = graph.nodes.match("Institution", name=instName)
            if institution is None:
                institution = Node("Institution", name=instName)
                for p in json.load(open(j, 'r', encoding='latin-1')):
                    if p is not None:
                        author = graph.nodes.match("Author", name=p['name'])
                        if author.first() is not None:
                            continue
                        author = Node("Author", name=p['name'],
                                      lattesurl=p['lattesurl'])
                        for synonyms in synonyms_list:
                            if p['name'] in synonyms:
                                author['aliases'] = synonyms
                        
                        graph.create(author)
                        graph.create(Relationship(author, "ASSOCIATED_TO", institution))
                        # del author
                        profList.append(author)
                        
            else:
                print("\tFilter load SKIPPED")
                for rel in institution.match(rel_type='ASSOCIATED_TO'):
                    profList.append(rel.start_node())
            del institution
        
        return profList


def removeAccents(data):
    """Remove accents from input string"""
    translationTable = str.maketrans("çäáâãèéêíóôõöúñ", "caaaaeeeiooooun", ":'`{}[])(@?!_-/")
    return data.lower().translate(translationTable)


@lru_cache(maxsize=1024)
def compareNames(a, b):
    """Compare two names using a heuristic method"""
    #TODO: improvement: use edit distance with a variable threshold based on name lenth
    dist_threshold = 5
    if a is None or b is None:
        return False
    if a == b:
        return True

    dist = 20
    #dist = distance.edit_distance(a, b)
        
    if dist <= dist_threshold:
        a_list = a.strip().split()
        b_list = b.strip().split()
        if not a_list or not b_list:
            return False
        if a_list[0] == b_list[0] and a_list[-1] == b_list[-1]:
            return True
    else:
        return False


def createCypherFiles(queue):
    """"Creates cypher files to be inserted into graph"""
    logger = logging.getLogger('cypherWriter')

    while True:
        pubAttrs = queue.get()
        if pubAttrs == -1:
            break

        title = pubAttrs.get("title")
        pubKey = uuid.uuid4().__str__()
        logger.info("New publication title: %s" % title.encode('latin-1', errors='ignore'))        
        query = ''

        for aName in pubAttrs["author"]:
            query += "MATCH (a:Author {name: '%s})' \n" % aName
            query += "MATCH (p:Publication {title: '%s'}) \n" % title
            query += "MERGE (a)-[r:AUTHORING]->(p)\n"
        with open(os.path.join(rootRepo, pubKey)+'.cypher', 'w', encoding='latin-1') as output:
            output.write(query)


def insertIntoGraph(pubAttrs):
    """Insert queue content into graph"""

    local_graph = Graph(CONFIG['neo4j']['url'],
              user=CONFIG['neo4j']['username'],
              password=CONFIG['neo4j']['password'],
              bolt=True
             )
           
    if pubAttrs is None or pubAttrs['type'] == 'www':
        del pubAttrs
        return

    matcher = NodeMatcher(local_graph)
    
    new_pub = matcher.match("Publication", title=pubAttrs.get("title")).first()
    if new_pub is None:        
        new_pub = Node()
        for att in pubAttrs.keys():
            new_pub[att] = pubAttrs.get(att, -1)
        new_pub.add_label('Publication')        
    else:
        # Publication already inserted
        del pubAttrs
        return
    
    attrs_author = pubAttrs.get('author')
    if not isinstance(attrs_author, list):
        attrs_author = list()
        attrs_author.append(pubAttrs.get('author'))

    can_insert = False
    for name in attrs_author:
        if name is None:
            continue
       #if author_matcher.match("Author").where("`" + name.replace("'", "\\'") + "` IN _.aliases").first() is not None:
        for node in matcher.match("Author"):
            if name in node.get('aliases', []) or name == node.get('name'):
                can_insert = True
                break
        if can_insert == True:
            break

    if not can_insert:
        return
    else:
        logger.info("Creating new publication ("+ pubAttrs.get("type", "") +"): " + pubAttrs.get("title", ""))
    
    for aName in attrs_author:
        author = None
        if aName is None:
            continue
        for authorNode in list(matcher.match("Author")):
            #if compareNames(removeAccents(authorNode['name']), removeAccents(aName)):
            if authorNode['name'].find(aName) >= 0:
                author = authorNode                
                break
            if authorNode.get('aliases') is not None and aName in authorNode['aliases']:
                author = authorNode                
                break             

        if author is None:
            author = Node("Author", name=aName)
            for synonyms in synonyms_list:
                if aName in synonyms:
                    author['aliases'] = synonyms
                    break
                        
            local_graph.create(author)
            logger.debug("New author: %s" % aName)
        rel_authoring = Relationship(author, "AUTHORING", new_pub)
        
        local_graph.create(rel_authoring)
        logger.debug("!!! Relationship created: " + new_pub.get('title'))        
        
        
            
    
# ================================================================
def runAlgorithms():
    graph = Graph(CONFIG['neo4j']['url'],
              user=CONFIG['neo4j']['username'],
              password=CONFIG['neo4j']['password'],
              bolt=True
             )
    betweenness = """
    CALL algo.betweenness('Author','COAUTHOR',{direction:'out', write:true, writeProperty:'centrality_b'})
YIELD nodes, minCentrality, maxCentrality, sumCentrality, loadMillis, computeMillis, writeMillis;
    """
    
    closeness = """
    CALL algo.closeness('Author', 'COAUTHOR', {write:true, writeProperty:'centrality_c'})
YIELD nodes,loadMillis, computeMillis, writeMillis;
    """
    
    connected_component = """
    CALL algo.unionFind('Author', 'COAUTHOR', {write:true, partitionProperty:"partition"})
YIELD nodes, setCount, loadMillis, computeMillis, writeMillis;
    """
    
    diameter = """
    MATCH p=(a:Author)-[r:COAUTHOR*1..10]->(b:Author)
WHERE size( (b)-[:COAUTHOR]->() ) = 0
RETURN p, length(p) as len
ORDER BY len DESC
LIMIT 10;
    """
    
    shortest_path = """
    """

    for query in [diameter, connected_component, closeness, betweenness]:
        graph.run(query)


def load_synonyms(attrs):
    if attrs['key'].find("homepages") >= 0:
        synonyms = attrs.get("author")
        if synonyms is not None:
            synonyms_list.append(synonyms)

def save_profiling_summary():
    from pympler import summary, muppy
    mem_summary = summary.summarize(muppy.get_objects())
    rows = summary.format_(mem_summary)
    with open("dblpparse-debug_info.txt", 'w') as fp:
        fp.write('\n'.join(rows)) 
        
def update_aliases():
    local_graph = Graph(CONFIG['neo4j']['url'],
              user=CONFIG['neo4j']['username'],
              password=CONFIG['neo4j']['password'],
              bolt=True
             )
    
    for author in local_graph.nodes.match("Author"):
        for entry in synonyms_list:
            if author.get('name') in entry:
                author['aliases'] = entry
                local_graph.push(author)
def create_synonyms(gzip_name):
    start_time = time.time()
    if os.path.isfile(CONFIG['dblp']['synonims_filename']):
        print("Skipping aliases load")
        with open(CONFIG['dblp']['synonims_filename'], encoding='latin-1') as fp:            
            synonyms_list = json.load(fp)
    else:
        with gzip.open(gzip_name, mode='rt', encoding='latin-1') as source:
                print("Loading aliases ...")
                parser = make_parser() 
                
                parser.setContentHandler(DBLPContentHandler(callback=load_synonyms, pubtype='www'))    
                parser.parse(source)

                with open(CONFIG['dblp']['synonims_filename'], 'w') as fp:                
                    json.dump(synonyms_list, fp)

    update_aliases()
    elapsed_time = time.time() - start_time
    print("Aliases loaded in ", datetime.timedelta(seconds=elapsed_time))
                

# ================================================================
def main(gzip_name):
    """main method"""
    print("Starting parse of file %s at %s" % (gzip_name, time.ctime()))
    start_time = time.time()
    
    loadSeeds()
    print("Author filter loaded")
    
    #queue = multiprocessing.Queue()
    downloadDBLPdump()

    print("Ready to parse")
    logger.info("Ready to parse")
    
    logger.info("Parsing ...")
    logger.info("Loading synonims ...")
    
    create_synonyms(gzip_name)

    #pool = mp.Pool(processes = os.cpu_count())    
    pool = mp.Pool(processes = 1)
    with gzip.open(gzip_name, mode='rt', encoding='latin-1') as source:
        print("Parsing XML ...")
        parser = make_parser()    
        #parser.setContentHandler(DBLPContentHandler(pool=pool, callback=insertIntoGraph))
        parser.setContentHandler(DBLPContentHandler(callback=insertIntoGraph))
        
        parser.parse(source)    
    #    print("Parsing XML ...")
    #    parser = make_parser()    
    #    parser.setContentHandler(DBLPContentHandler(callback=insertIntoGraph))
    #    parser.parse(source)

    elapsed_time = time.time() - start_time
    print("Execution time ", datetime.timedelta(seconds=elapsed_time))
    print("Finish")


if __name__ == "__main__":
    main(URL)
