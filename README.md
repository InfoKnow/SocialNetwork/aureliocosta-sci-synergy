# SCI-synergy
Social Network Analysis to Improve Scientific Collaboration

This project delivers a tool to conduct analysis based on collaboration network of scientific publication authors. Initially, [DBLP](http://dblp.uni-trier.de/) as publication data source. 

In frontend we use material design and [D3JS](https://d3js.org).

In backend  we use [Flask](https://palletsprojects.com/p/flask/) as API Server and python 

The database is powered by [Neo4j](https://neo4j.com) community.



# Architecture
The architecture is as follow:  
![Sci-Synergy Architecture](sci-synergy arch extended.png)

# Prerequisites to run local
To run SCI-synergy by yourself, the prerequisites are having Docker container runtime and execute the following:
* git clone https://gitlab.com/InfoKnow/SocialNetwork/aureliocosta-sci-synergy.git
* cd aureliocosta-sci-synergy
* docker-compose -f docker-compose.yml up

We are working in a implementation to run on kubernetes

# Current implementation
The current implementation can be accessed using this address: http://165.227.113.212/
